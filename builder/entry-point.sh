#!/usr/bin/env sh

# CONTENT=$(pandoc "$2") envsubst '$CONTENT' < "$1" | prettyhtml --stdin
CONTENT=$(pandoc "$2") \
    envsubst '$CONTENT' < "$1" \
    | prettyhtml --stdin --quiet
#    2> /tmp/stderr

# >&2 grep -v '<stdin>: no issues found' /tmp/stderr 
