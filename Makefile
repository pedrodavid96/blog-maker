# COMPILER = docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pandoc/core
COMPILER := docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pedro/blogbuilder:0.1.0

all: out/index.html

out/%.html: src/%.md template.html.subst | out
	$(COMPILER) template.html.subst $< > $@ 2> /dev/null

out:
	mkdir ./out

.PHONY: clean
clean:
	rm -rf ./out
